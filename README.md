Main files:

index.js - to start the server

html/index.html - the html code of index page

styles/scss/main.scss - the scss for index.html
styles/css/main.css - the css file created by main.scss

Important command:

To monitor changes of scss file:

sass --watch ./styles/scss/main.scss ./styles/css/main.css
