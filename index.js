var express = require('express');
var app = express();
var path = require('path')
app.use(express.static(path.join(__dirname,'styles')))
app.get('/', function(res, rep) {
    rep.sendFile(__dirname + '/html/index.html')
    // rep.send('Hello, word!');
});

app.listen(3000);